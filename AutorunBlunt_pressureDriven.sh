#!/usr/bin/env bash
###############################################################################
### 1. Read a `*.csv`, with various simulation data, for each line in it –
### 2. Create and move into a simulation folder using bash while loop
### 3. Copy/relocate all necessary folders for running openFOAM
### 4. Format & update the required OpenFOAM files with data from the `*.csv`
### 5. Pass onto a) blockMesh, b) meshing utilities, c) scaling d) simpleFOAM
### 6. Save ouputs in the `logs` folder; Cleanup the directory
### 7. Move onto next simulation (next line of `*.csv`)
### • the `||exit` switches can be switched on for debugging.
###############################################################################

STUDYNAME=LHD_input_10simulations
INPUTCSV=$STUDYNAME.csv
MASTERSCRIPTSPATH=.
MODELDIRECTORYPATH=$MASTERSCRIPTSPATH/../../modelOFDirectoryBluntBMD
INPUTBLOCKMESHDICT=$MODELDIRECTORYPATH/system/blockMeshDict0.005X0.010Y
BUPFORMATTERSCRIPT=$MASTERSCRIPTSPATH/../../bUpFormatterBluntBMD_pressureDriven.sh
INPUTCONTROLDICT=$MODELDIRECTORYPATH/system/controlDict
INPUTFVSOLUTION=$MODELDIRECTORYPATH/system/fvSolution #_relaxationFactors0.95
INPUTDECOMPOSEPARDICT=$MODELDIRECTORYPATH/system/decomposeParDict
INPUTTRANSPORTPROPERTIES=$MODELDIRECTORYPATH/constant/transportProperties_K_n
# FOAMINSTDIR=/opt/openfoam8;#oF8 in UBUNTU
FOAMINSTDIR=/Volumes/OpenFOAM-v2206/OpenFOAM-v2206;# mounted at
# this location in macOS 13
OFPOSTPROCESSINGDIR=$FOAMINSTDIR/etc/caseDicts/postProcessing
###############################################################################

mkdir -p $STUDYNAME;
rsync ../Data/$INPUTCSV $STUDYNAME/ #|| exit && echo "problem" ;
cd $STUDYNAME #|| exit && echo "didn't change directory";

exec 3</dev/tty || exec 3<&0 #make FD 3 point to the TTY or stdin (as fallback)
#https://stackoverflow.com/questions/41650892/why-redirect-stdin-inside-a-while-read-loop-in-bash/41652573#41652573

awk -F, 'NR > 1 && NR < 201' $INPUTCSV | #choose rows
while IFS="," read -r \
directory Rb Rs Lu Ll p_inlet_kPa K n; do
    mkdir -p "$directory"; #`directory` is parsed from previous command
    cd "$directory" && touch "$directory".foam;
    mkdir -p logs/ 0/ constant/ system/;
## BLOCKMESHDICT ##############################################################
    cp -r $INPUTBLOCKMESHDICT ./system/BMD &&
    awk -v Rb="$Rb" -v Rs="$Rs" -v Lu="$Lu" -v Ll="$Ll" '{
        if($0 ~ "//set Rbig here")
            {print $1"\t"Rb";";//set Rbig here}
        else if($0 ~ "//set Rsmall here")
            {print $1"\t"Rs";";//set Rsmall here}
        else if($0 ~ "//set Lupper here")
            {print $1"\t"Lu";";//set Lupper here}
        else if($0 ~ "//set Llower here")
            {print $1"\t"Ll";";//set Lupper here}
        else {print $0}}' system/BMD >| system/blockMeshDict || exit;

    cp -r $INPUTCONTROLDICT ./system/controlDict;
    cp -r $INPUTFVSOLUTION ./system/fvSolution;
    cp -r $MODELDIRECTORYPATH/system/createPatchDict ./system/;
    cp -r $MODELDIRECTORYPATH/system/extrudeMeshDict ./system/;
    cp -r $MODELDIRECTORYPATH/system/fvSchemes ./system/;
    cp -r $MODELDIRECTORYPATH/constant/turbulenceProperties ./constant/;
#### TRANSPORTPROPERTIES #####################################################
    rm -rf ./constant/transportProperties && \
    cp -r $INPUTTRANSPORTPROPERTIES ./constant/transportProperties_K_n &&
    awk -v K="$K" -v n="$n" '{gsub(/K\;\/\//, K";//"); gsub(/n\;\/\//, n";//");
          print $0;}' constant/transportProperties_K_n > constant/\
transportProperties && rm -f transportProperties_K_n || exit;

# openFOAM commands start here ###############################################
    #source $foamInstDir/etc/bashrc </dev/null;#^-------------------^ SC1090:
#Can't follow non-constant source. Use a directive to specify location.
    blockMesh > logs/"${PWD##*/}"_blockMesh.log #|| exit;
    #makes the base mesh using the parameters from the `*.csv`
    extrudeMesh > logs/"${PWD##*/}"_extrudeMesh.log #|| exit;
    #takes a 2D mesh and converts into wedge Mesh.
    createPatch -overwrite #| tee logs/"${PWD##*/}"_createPatch.log || exit;
    #uses createPatchDict and removes patches having `0` faces.
    checkMesh -allTopology -allGeometry >| \
    logs/"${PWD##*/}"_checkMesh.log #|| exit;
# making a `U` & `p` in `0` from `constant/polyMesh/boundary`
    awk -v p_inlet="$p_inlet_kPa" '{
          gsub("p_inlet=10","p_inlet = "p_inlet); #p_inlet_kPa from the `*.csv`
          print $0}' $BUPFORMATTERSCRIPT > \
    bUpFormatter.sh && source bUpFormatter.sh </dev/null #|| exit;

    transformPoints -scale '(0.001 0.001 0.001)' #|| exit;# scales the polyMesh,
# `p` or`U` is not affected; not needed if the `*.csv` is coded in millimeter.

    #Running openFOAM solvers and plot residuals using `gnuplot`
    # source $MASTERSCRIPTSPATH/mpirunOF.sh;#as mpirun devours the whole `stdin`
    # decomposePar -force </dev/null && \
    # mpirun -np 8 renumberMesh -parallel -overwrite <&3;
    # rm -rf logs/foamlog && \
    # mpirun -np 8 simpleFoam -parallel > logs/foamlog <&3;
    # # pyFoamPlotWatcher.py logs/foamlog;
    # reconstructPar <&3;

    simpleFoam > foamLog."${PWD##*/}";
    # source $MASTERSCRIPTSPATH/scriptPlottingResidualsMagWSS.sh
    # source $MASTERSCRIPTSPATH/scriptPlottingResiduals.sh;

## manual postProcessing, comment out the things you don't need ###############
#    cp -v $OFPOSTPROCESSINGDIR/fields/CourantNo system/ && \
#postProcess -func CourantNo || exit;# -latestTiPme;
#    simpleFoam -postProcess -func wallShearStress #|| exit;
#    cp -r $OFPOSTPROCESSINGDIR/flowRate/flowRatePatch ./system/ && \
#postProcess -func "flowRatePatch(name=outlet)" #|| exit;
#    cp -v $OFPOSTPROCESSINGDIR/surfaceFieldValue/patchAverage ./system/ && \
#postProcess -func "patchAverage(name=outlet,U,nu)" #|| exit;
#    postProcess -func "flowRatePatch(name=inlet)" > logs/\
#"${PWD##*/}"inlet_flowrate.log </dev/null || exit;

    cd ..;#has to exit to the upper level to be able to run the next case
done;
exec 3<&- ## close FD 3 when done.

#Cleaning blockMeshDict Generated stuffs
cd .. && find $STUDYNAME -type d -name "dynamicCode" -exec rm -rf '{}' \;
