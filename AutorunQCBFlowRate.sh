#!/usr/bin/env bash
###### Copy/relocate all necessary folders for running openFOAM
###### Pass onto a) blockMesh, b) snappyHexMesh c) relocate d) autoPatch ####
###################################################
################################################################################
STUDYNAME=RTDBlunted
INPUTCSV=$STUDYNAME.csv
MASTERSCRIPTSPATH=/home/massisenergy/autoMount/Data_Documents/Dox_ownCloud/\
openFoamDataSR/openfoam_scripts
MODELDIRECTORYPATH=$MASTERSCRIPTSPATH/modelDirQuarterCylBlunted
BUPFORMATTERSCRIPT=$MODELDIRECTORYPATH/bUpFormatterQCBFlowRate.sh
INPUTCONTROLDICT=$MODELDIRECTORYPATH/system/controlDict
INPUTFVSOLUTION=$MODELDIRECTORYPATH/system/fvSolution
INPUTTRANSPORTPROPERTIES=$MODELDIRECTORYPATH/constant\
/transportProperties_K_n_tau0
FOAMINSTDIR=/opt/OpenFOAM/OpenFOAM-9;#of9 in ArchLinux
OFPOSTPROCESSINGDIR=$FOAMINSTDIR/etc/caseDicts/postProcessing
################################################################################

mkdir -p $STUDYNAME;
rsync -uv $INPUTCSV $STUDYNAME/ #|| exit && echo "problem" ;
cd $STUDYNAME #|| exit && echo "didn't change directory";

exec 3</dev/tty || exec 3<&0 #make FD 3 point to the TTY or stdin (as fallback)
#https://stackoverflow.com/questions/41650892/why-redirect-stdin-inside-a-while-read-loop-in-bash/41652573#41652573

awk -F, 'NR > 17' $INPUTCSV | #choose rows
while IFS="," read -r \
directory Rb Rs Rm Lu Ll flowRate K n deltaX deltaY tau0 Lu2; do
    mkdir "$directory"; #`directory` is parsed from previous command
    cd "$directory" && touch "$directory".foam;
    rm -rf logs/;
    mkdir -p logs/ 0/ constant/ system/;
### BLOCKMESHDICT ###### ####################################################
    if [[ ! -n "$Rm" ]]; then
        INPUTBLOCKMESHDICT=$MODELDIRECTORYPATH/system/blockMeshDict || exit;
    elif [[ ! -n "$Lu2" ]]; then
        INPUTBLOCKMESHDICT=$MODELDIRECTORYPATH/system/\
blockMeshDict_LuSlanted || exit;
    elif [[ -n "$Lu2" ]]; then
        INPUTBLOCKMESHDICT=$MODELDIRECTORYPATH/system/\
blockMeshDict_LuSlantedLu2 || exit;
    fi;
    echo using the "$INPUTBLOCKMESHDICT";
    rsync -a $INPUTBLOCKMESHDICT ./system/BMD &&
    awk -v deltaX="$deltaX" -v deltaY="$deltaY" \
    -v Rb="$Rb" -v Rs="$Rs" -v Rm="$Rm" -v Lu="$Lu" -v Ll="$Ll" -v Lu2="$Lu2" \
    '{
        if ($1 ~ /(deltax_block0)/)
            {print $1"\t"deltaX";"}
        else if($1 ~ /(deltay_block0)/)
            {print $1"\t"deltaY";"}
        else if($0 ~ "//set Rbig here")
            {print $1"\t"Rb";";//set Rbig here}
        else if($0 ~ "//set Rsmall here")
            {print $1"\t"Rs";";//set Rsmall here}
        else if($0 ~ "//set Rmiddle here")
            {print $1"\t"Rm";";//set Rmiddle here}
        else if($0 ~ "//set Lupper here")
            {print $1"\t"Lu";";//set Lupper here}
        else if($0 ~ "//set Llower here")
            {print $1"\t"Ll";";//set Lupper here}
        else if($0 ~ "//set Lu2 here")
            {print $1"\t"Lu2";";//set Lu2 here}
        else {print $0}}' system/BMD >| system/blockMeshDict || exit;

    rsync -au $INPUTCONTROLDICT ./system/controlDict;
    rsync -a $INPUTFVSOLUTION ./system/fvSolution;
    # rsync -a $INPUTDECOMPOSEPARDICT ./system/decomposeParDict;
    # rsync -a $MODELDIRECTORYPATH/system/createPatchDict ./system/;
    # rsync -a $MODELDIRECTORYPATH/system/extrudeMeshDict ./system/;
    rsync -a $MODELDIRECTORYPATH/system/fvSchemes ./system/;
    # rsync -a $MODELDIRECTORYPATH/system/meshQualityDict ./system/;
    rsync -a $MODELDIRECTORYPATH/constant/turbulenceProperties ./constant/;
#### TRANSPORTPROPERTIES ######################################################
    if [[ ! -n "$tau0" ]]; then 
        rsync -au $INPUTTRANSPORTPROPERTIES ./constant/transportProperties_K_n_tau0&&
        sed -i s/k_powerLaw/$K/g ./constant/transportProperties_K_n_tau0&& 
	sed -i s/n_powerLaw/$n/g ./constant/transportProperties_K_n_tau0&& 
	awk '{if ($0~"_HerBulLaw") {print "// "$0}
	    else {print $0}}' constant/transportProperties_K_n_tau0> \
	constant/transportProperties && 
	rm -f transportProperties_K_n_tau0|| exit;
    elif [[ -n "$tau0" ]]; then 
        rsync -au $INPUTTRANSPORTPROPERTIES ./constant/transportProperties_K_n_tau0&&
        sed -i s/k_HerBulLaw/$K/g ./constant/transportProperties_K_n_tau0&& 
	sed -i s/n_HerBulLaw/$n/g ./constant/transportProperties_K_n_tau0&& 
	sed -i s/tau0_HerBulLaw/$tau0/g ./constant/transportProperties_K_n_tau0&& 
	awk '{if ($0~"_powerLaw") {print "// "$0}
	    else {print $0}}' constant/transportProperties_K_n_tau0> \
	constant/transportProperties && 
	rm -f transportProperties_K_n_tau0|| exit;
    fi;

#### openFOAM commands start here #############################################
    blockMesh | tee logs/blockMesh.log || exit;
    # extrudeMesh | tee logs/"${PWD##*/}"_extrudeMesh.log || exit;#takes a 2D
#mesh and converts into wedge Mesh
    # createPatch -overwrite | tee logs/"${PWD##*/}"_createPatch.log || exit;#uses
#createPatchDict and removes patches having `0` faces.
    # checkMesh -allTopology -allGeometry | tee \
    # logs/"${PWD##*/}"_checkMesh.log || exit;
    # making a `U` & `p` in `0` from `constant/polyMesh/boundary`
    awk -v flowRate="$flowRate" '{
          gsub("flowRate = \"1\"","flowRate = "flowRate);
          print $0}' $BUPFORMATTERSCRIPT > \
    bUpFormatterQCB.sh && source bUpFormatterQCB.sh </dev/null || exit;

    # transformPoints -scale '(0.001 0.001 0.001)' || exit;# scales the polyMesh,
# `p` or`U` is not affected; not needed if the `*.csv` is coded in millimeter.

    # #Running openFOAM solvers and plot residuals using `gnuplot`
    # # source $MASTERSCRIPTSPATH/mpirunOF.sh;#as mpirun devours the whole `stdin`
    # decomposePar -force </dev/null && \
    # mpirun -np 8 renumberMesh -parallel -overwrite <&3;
    # rm -rf logs/foamlog && \
    # mpirun -np 8 simpleFoam -parallel > logs/foamlog <&3;
    # # pyFoamPlotWatcher.py logs/foamlog;
    # reconstructPar <&3;

    simpleFoam > foamLog."${PWD##*/}"; 
    # source $MASTERSCRIPTSPATH/scriptPlottingResidualsMagWSS.sh
    # source $MASTERSCRIPTSPATH/scriptPlottingResiduals.sh;

    #postProcessing #################
#     rsync -v $OFPOSTPROCESSINGDIR/fields/CourantNo system/ && \
# postProcess -func CourantNo || exit;# -latestTiPme;
    # simpleFoam -postProcess -func wallShearStress || exit;# | tee \
# logs/"${PWD##*/}"_WSS.log </dev/null
#     rsync -a $OFPOSTPROCESSINGDIR/flowRate/flowRatePatch ./system/ && \
# postProcess -func "flowRatePatch(name=outlet)" || exit;# | \
# tee logs/"${PWD##*/}"_flowrate.log
#     postProcess -func "flowRatePatch(name=inlet)" > logs/\
# "${PWD##*/}"inlet_flowrate.log </dev/null || exit;
#     rsync -v $OFPOSTPROCESSINGDIR/surfaceFieldValue/patchAverage ./system/ && \
# postProcess -func "patchAverage(name=outlet,U,nu)" || exit;

    cd ..;#has to exit to the upper level to be able to run the next case
done;

exec 3<&- ## close FD 3 when done.
#Cleaning blockMeshDict Generated stuffs
cd .. && find $STUDYNAME -type d -name "dynamicCode" -exec rm -rf '{}' \;
# find $STUDYNAME -type d -name "processor*" -exec rm -r '{}' \;
