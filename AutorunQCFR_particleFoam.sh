#!/usr/bin/env bash

###############################################################################
MASTERSCRIPTSPATH=/home/massisenergy/autoMount/Data_Documents/Dox_ownCloud/\
openFoamDataSR/openfoam_scripts
MODELDIRECTORYPATH=$MASTERSCRIPTSPATH/modelDirQuarterCylParticleFoam
Ts1=3000
Te1=3001
Ts2=3001
Te2=3002
###############################################################################

rsync -a $MODELDIRECTORYPATH/system/controlDict_populatingWholeDomain \
    system/controlDict && 
rsync -a $MODELDIRECTORYPATH/system/{fvSchemes,fvSolution} system/ && 
rsync -a $MODELDIRECTORYPATH/constant/g constant/g ||exit && 
rsync -a $MODELDIRECTORYPATH/constant/cloudProperties_populatingWholeDomain \
constant/cloudProperties ||exit && 
nohup particleFoam > foamLog."${PWD##*/}"_populatingWholeDomain ||exit && 
rsync -a $MODELDIRECTORYPATH/system/controlDict_deltaT.025_500to502 \
    system/controlDict && 
rsync -a $MODELDIRECTORYPATH/constant/cloudProperties_deltaT.025_500to502 \
constant/cloudProperties ||exit &&
# nohup particleFoam > foamLog."${PWD##*/}"_deltaT.025_500to501 ||exit && 
# nohup foamListTimes | while read i; 
#     do rsync -avh 0/shearStress $i/; done ||exit && 
# nohup foamToVTK -time '501:502' -fields '(U sherStress)' ||exit && 
# nohup mv VTK/ VTK_501to502delta0.025/ ||exit && 
# nohup foamToVTK -time '500:501' -fields '(U shearStress)' && 
# nohup mv VTK/ VTK_500to501delta0.025/

nohup foamListTimes -time "$Ts1":"$Te2" |while read i; do rsync -a \
    0/shearStress $i/; done ||exit && nohup foamToVTK -time "$Ts1":"$Te1" \
    -fields '(U shearStress)' ||exit && nohup mv VTK/ \
    VTK_"$Ts1"to"$Te1"deltaT0.025/ ||exit && nohup foamToVTK -time \
    "$Ts2":"$Te2" -fields '(U shearStress)' ||exit && nohup \
    mv VTK/ VTK_"$Ts2"to"$Te2"deltaT0.025/
