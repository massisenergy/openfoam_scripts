
#!/usr/bin/env bash
# ################################# Description ###############################
# these AWK-SED scripts takes U or p file needed for openFOAM, found in `0`
# directory of every project and formats a new U or p file, with input from
# a suitable `constant/polyMesh/boundary` file for assignement of
# patches (boundary conditions)
# NOTE: works with `blockMeshDict`
# #############################################################################

# awk or sed script to format the 'boundary'
#mv constant/polyMesh/boundary constant/polyMesh/boundary_backup;
#awk '$0!~"inGroups" {print;}
#' constant/polyMesh/boundary_backup > constant/polyMesh/boundary_backup_1;
#
#awk '
#    NR==23 && $0~"type" {gsub("patch;","patch;//inlet"); print;}
#    NR==29 && $0~"type" {gsub("patch;","patch;//outlet"); print;}
#    NR==35 && $0~"type" {gsub("wall;","wall;//wall"); print;}
#    NR==41 && $0~"type" {gsub("wall;","wall;//wall"); print;}
#    NR==47 && $0~"type" {gsub("wall;","wall;//wall"); print;}
#    NR==53 && $0~"type" {gsub("wedge;","wedge;//wedgeBack"); print;}
#    NR==59 && $0~"type" {gsub("wedge;","wedge;//wedgeFront"); print;}
#
#    $0~"type" && NR>=21 {next;}
#    $0!~"inlet" || $0!~"outlet" || $0!~"nozzleWall" || $0!~"back" \
#    $0!~"front" && NR>=21  {print;}
#' constant/polyMesh/boundary_backup_1 > constant/polyMesh/boundary;

mv constant/polyMesh/boundary constant/polyMesh/boundary_backup;
sed -e  '/inGroups/d' \
    -e '21s/patch;/patch;\/\/inlet/g' \
    -e '27s/patch;/patch;\/\/outlet/g' \
    -e 's/wall;/wall;\/\/wall/g' \
    -e 's/wedge;/wedge;\/\/wedge/g' \
    -e 's/symmetryPlane;/symmetryPlane;\/\/symmetryPlane/g' \
    constant/polyMesh/boundary_backup > constant/polyMesh/boundary

###############################################################################

###############################################################################
# next series of scripts formats `U` & `p` respectively, using `boundary`.
# this is for fromatting the U file
#cat 0/U_pressureDriven | sed -e '/boundaryField/q' >| U
cat ../../modelOFDirectoryBluntBMD/0/U_flowRateDriven |
    sed -e '/boundaryField/q' >| 0/U
echo "{" >> 0/U

awk 'BEGIN{FS=" "; flowRate = "1"}{
    if (NR <=19) {
        next;
    } else if ((NF == 2) && ($1 == "type") && ($2 ~ "inlet")) {
        print "\t\t"$1"\t\t\t""flowRateInletVelocity;"
        print "\t\t""volumetricFlowRate""\t\t\t""constant "flowRate";"
        print "\t\t""value""\t\t\t""uniform (0 0 0);"
    } else if ((NF == 2) && ($1 == "type") && ($2 ~ "outlet")) {
        print "\t\t"$1"\t\t\t""zeroGradient;"
    } else if ((NF == 2) && ($1 == "type") && ($2 ~ "wall;")) {
        print "\t\t"$1"\t\t\t""noSlip;"
    } else if ((NF == 2) && ($1 == "type") && ($2 ~ "wedge")) {
        gsub("empty;","wedge;"); print;
    } else if ((NF == 2) && ($1 == "physicalType" || "nFaces" || "startFace")) {
        next;
    } else if (($1 == "(") || ($1 == "inGroups")) {
            next;
    } else if ((NF == 1) && ($0 == ")")) {
        print "}";
    } else {
        print;
    }
}' constant/polyMesh/boundary >> 0/U
###############################################################################

###############################################################################
#this is for fromatting the p file
#cat 0/p_pressureDriven | sed -e '/boundaryField/q' >| p
cat ../../modelOFDirectoryBluntBMD/0/p_flowRateDriven |
    sed -e '/boundaryField/q' >| 0/p
echo "{" >> 0/p

awk 'BEGIN{FS=" "}{
    if (NR <=19) {
        next;
    } else if ((NF == 2) && ($1 == "type") && ($2 ~ "inlet")) {
        print "\t\t"$1"\t\t\t""zeroGradient;"

    } else if ((NF == 2) && ($1 == "type") && ($2 ~ "outlet")) {
        print "\t\t"$1"\t\t\t""fixedValue;"
        print "\t\t""value""\t\t\t""uniform 0;"
    } else if ((NF == 2) && ($1 == "type") && ($2 ~ "wall;")) {
        print "\t\t"$1"\t\t\t""zeroGradient;"
    } else if ((NF == 2) && ($1 == "type") && ($2 ~ "wedge")) {
        gsub("empty;","wedge;"); print;
    } else if ((NF == 2) && ($1 == "physicalType" || "nFaces" || "startFace")) {
        next;
    } else if (($1 == "(") || ($1 == "inGroups")) {
        next;
    } else if ((NF == 1) && ($0 == ")")) {
        print "}";
    } else {
        print;
    }
}' constant/polyMesh/boundary >> 0/p
##############################################################################
