/*--------------------------------*- C++ -*----------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     | Website:  https://openfoam.org
    \\  /    A nd           | Version:  8
     \\/     M anipulation  |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    object      blockMeshDict;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

convertToMeters 0.001;//`0.001` sets unit to millimeter.

// This part is for identification and modification of following parameters
// with external values, by programs such as `AWK`
Rb	2.135;//2.64;
Rs	0.205;//0.1;
Lu	5;//14.1;
Ll	6.35;//0.76;
// Rm  1.725;//1.54;//set Rmiddle here
// Lm  20;

xAxis          0;
ymin_b0        0;
zAxis          0;
xmax_b0        $Rs;//1;
ymax_b0        $Ll;//3;
zmax_b0        $xmax_b0;
xarc_b0        #calc "0.7071 * $xmax_b0";
zarc_b0        $xarc_b0;

xmid_b0        #calc "($xmax_b0 / 2)";//point at the middle of the region connecting three blocks
zmid_b0        $xmid_b0;

arc12x_b0      #calc "$xmax_b0 * 0.382680";
arc12z_b0      #calc "$xmax_b0 * 0.923879";


xmin_b1        $xmax_b0;
ymin_b1        $Ll;
// xmax_b1        $Rm;//1;
xmax_b1        $Rb;//1;
// ymax_b1        #calc "($Ll + $Lm)";//3;
ymax_b1        #calc "($Ll + $Lu)";//3;
zmax_b1        $xmax_b1;
xarc_b1        #calc "0.7071 * $xmax_b1";
zarc_b1        $xarc_b1;

xmid_b1        #calc "($xmax_b1 / 2)";//point at the middle of the region connecting three blocks
zmid_b1        $xmid_b1;

arc12x_b1      #calc "$xmax_b1 * 0.382680";
arc12z_b1      #calc "$xmax_b1 * 0.923879";

vertices
(    //Y==0 First cuboid (inner)
    ($xAxis        $ymin_b0      $zAxis)//0
    ($xAxis        $ymin_b0      $zmid_b0)//1
    ($xmid_b0      $ymin_b0      $zmid_b0)//2
    ($xmid_b0      $ymin_b0      $zAxis)//3
    //third cuboid (Z side)
    ($xAxis        $ymin_b0      $zmax_b0)//4
    ($xarc_b0      $ymin_b0      $zarc_b0)//5
    //second cuboid (X side)
    ($xmax_b0      $ymin_b0      $zAxis)//6
    //Y==ymax
    ($xAxis        $ymax_b0      $zAxis)//7
    ($xAxis        $ymax_b0      $zmid_b0)//8
    ($xmid_b0      $ymax_b0      $zmid_b0)//9
    ($xmid_b0      $ymax_b0      $zAxis)//10
    ($xAxis        $ymax_b0      $zmax_b0)//11
    ($xarc_b0      $ymax_b0      $zarc_b0)//12
    ($xmax_b0      $ymax_b0      $zAxis)//13
    //block1 bottom plane (the axis point is same with block 0 top plane axis)
    ($xAxis        $ymin_b1      $zAxis)//7==14
    ($xAxis        $ymin_b1      $zmid_b1)//15
    ($xmid_b1      $ymin_b1      $zmid_b1)//16
    ($xmid_b1      $ymin_b1      $zAxis)//17
    ($xAxis        $ymin_b1      $zmax_b1)//18
    ($xarc_b1      $ymin_b1      $zarc_b1)//19
    ($xmax_b1      $ymin_b1      $zAxis)//20
    //block 1 top plane
    ($xAxis        $ymax_b1      $zAxis)//21
    ($xAxis        $ymax_b1      $zmid_b1)//22
    ($xmid_b1      $ymax_b1      $zmid_b1)//23
    ($xmid_b1      $ymax_b1      $zAxis)//24
    ($xAxis        $ymax_b1      $zmax_b1)//25
    ($xarc_b1      $ymax_b1      $zarc_b1)//26
    ($xmax_b1      $ymax_b1      $zAxis)//27
);

blocks
(
    hex (0 1 2 3 7 8 9 10)       (3 3 200) simpleGrading (1 1 1)
    hex (2 5 6 3 9 12 13 10)     (3 3 200) simpleGrading (1 1 1)
    hex (1 4 5 2 8 11 12 9)      (3 3 200) simpleGrading (1 1 1)

    hex (14 15 16 17 21 22 23 24)(20 20 50)edgeGrading (1 1 1 1 1 1 1 1 4 4 4 4)
    hex (16 19 20 17 23 26 27 24)(20 20 50)edgeGrading (1 1 1 1 1 1 1 1 4 4 4 4)
    hex (15 18 19 16 22 25 26 23)(20 20 50)edgeGrading (1 1 1 1 1 1 1 1 4 4 4 4)
);

edges
(
      arc  4 5   ($arc12x_b0 $ymin_b0 $arc12z_b0)//arc  1 2 (0.38268 0 0.923879)
      arc  11 12 ($arc12x_b0 $ymax_b0 $arc12z_b0)//arc  5 6 (0.38268 3 0.923879)
      arc  5 6   ($arc12z_b0 $ymin_b0 $arc12x_b0)//arc  2 3 (0.923879 0 0.38268)
      arc  12 13 ($arc12z_b0 $ymax_b0 $arc12x_b0)//arc  7 6 (0.923879 3 0.38268)
      arc  18 19 ($arc12x_b1 $ymin_b1 $arc12z_b1)
      arc  19 20 ($arc12z_b1 $ymin_b1 $arc12x_b1)
      arc  25 26 ($arc12x_b1 $ymax_b1 $arc12z_b1)
      arc  26 27 ($arc12z_b1 $ymax_b1 $arc12x_b1)
);

boundary
(
    inlet
    {
        type patch;
        faces
        (
            (21 22 23 24)(23 26 27 24)(22 25 26 23)
        );
    }
    outlet
    {
        type patch;
        faces
        (
            (0 3 2 1)    (2 3 6 5)     (1 2 5 4)
        );
    }
    inlet_b0//to be merged with outlet_b1Wall
    {
        type    patch;
        faces
        (
            (7 8 9 10)   (8 11 12 9)   (9 12 13 10)
        );
    }
    middleNozzleWall//outlet_b1Wall
    {
        type    wall;
        faces
        (
            (14 17 16 15)(15 16 19 18)(16 17 20 19)
        );
    }
    lowerNozzleWall
    {
        type wall;
        faces
        (
            (5 6 13 12)  (4 5 12 11)
        );
    }
    upperNozzleWall
    {
        type wall;
        faces
        (
            (19 20 27 26)(18 19 26 25)
        );
    }
    symmetryX
    {
        type    symmetryPlane;
        faces
        (
            (7 10 3 0)   (10 13 6 3)
            (21 24 17 14)(24 27 20 17)
        );
    }
    symmetryZ
    {
        type    symmetryPlane;
        faces
        (
            (7 0 1 8)    (8 1 4 11)
            (21 14 15 22)(22 15 18 25)
        );
    }

);

mergePatchPairs
(
    (middleNozzleWall    inlet_b0)
);

// ************************************************************************* //
