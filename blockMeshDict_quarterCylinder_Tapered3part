/*--------------------------------*- C++ -*----------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     | Website:  https://openfoam.org
    \\  /    A nd           | Version:  7
     \\/     M anipulation  |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    object      blockMeshDict;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

convertToMeters 0.001;//`0.001` sets unit to millimeter.

// This part is for identification and modification of following parameters
// with external values, by programs such as `AWK`
Rb	2.64;//2.135;
Rs	0.1;//0.14;
Lu	14.1;//10.31;
Ll	0.76;//19.76;
Rm  1.725;//1.54;//set Rmiddle here
Lm  20;

xAxis          0;
ymin_b0        0;
zAxis          0;
xmax_b0        $Rs;//1;
ymax_b0        $Ll;//3;
zmax_b0        $xmax_b0;
xarc_b0        #calc "0.7071 * $xmax_b0";
zarc_b0        $xarc_b0;

xmid_b0        #calc "($xmax_b0 / 2)";//point at the middle of the region connecting three blocks
zmid_b0        $xmid_b0;

arc12x_b0      #calc "$xmax_b0 * 0.382680";
arc12z_b0      #calc "$xmax_b0 * 0.923879";


xmin_b1        $xmax_b0;
ymin_b1        $Ll;
xmax_b1        $Rm;//1;
ymax_b1        #calc "($Ll + $Lm)";//3;
zmax_b1        $xmax_b1;
xarc_b1        #calc "0.7071 * $xmax_b1";
zarc_b1        $xarc_b1;

xmid_b1        #calc "($xmax_b1 / 2)";//point at the middle of the region connecting three blocks
zmid_b1        $xmid_b1;

arc12x_b1      #calc "$xmax_b1 * 0.382680";
arc12z_b1      #calc "$xmax_b1 * 0.923879";


xmin_b2        $xmax_b1;
ymin_b2        #calc "($Ll + $Lm)";
xmax_b2        $Rb;//1;
ymax_b2        #calc "($Ll + $Lu + $Lm)";//3;
zmax_b2        $xmax_b2;
xarc_b2        #calc "0.7071 * $xmax_b2";
zarc_b2        $xarc_b2;

xmid_b2        #calc "($xmax_b2 / 2)";//point at the middle of the region connecting three blocks
zmid_b2        $xmid_b2;

arc12x_b2      #calc "$xmax_b2 * 0.382680";
arc12z_b2      #calc "$xmax_b2 * 0.923879";


vertices
(    //Y==0 First cuboid (inner)
    ($xAxis        $ymin_b0      $zAxis)//0
    ($xAxis        $ymin_b0      $zmid_b0)//1
    ($xmid_b0      $ymin_b0      $zmid_b0)//2
    ($xmid_b0      $ymin_b0      $zAxis)//3
    //third cuboid (Z side)
    ($xAxis        $ymin_b0      $zmax_b0)//4
    ($xarc_b0      $ymin_b0      $zarc_b0)//5
    //second cuboid (X side)
    ($xmax_b0      $ymin_b0      $zAxis)//6
    //Y==ymax
    ($xAxis        $ymax_b0      $zAxis)//7
    ($xAxis        $ymax_b0      $zmid_b0)//8
    ($xmid_b0      $ymax_b0      $zmid_b0)//9
    ($xmid_b0      $ymax_b0      $zAxis)//10
    ($xAxis        $ymax_b0      $zmax_b0)//11
    ($xarc_b0      $ymax_b0      $zarc_b0)//12
    ($xmax_b0      $ymax_b0      $zAxis)//13
    //block 2 top plane
    ($xAxis        $ymax_b1      $zAxis)//14
    ($xAxis        $ymax_b1      $zmid_b1)//15
    ($xmid_b1      $ymax_b1      $zmid_b1)//16
    ($xmid_b1      $ymax_b1      $zAxis)//17
    ($xAxis        $ymax_b1      $zmax_b1)//18
    ($xarc_b1      $ymax_b1      $zarc_b1)//19
    ($xmax_b1      $ymax_b1      $zAxis)//20
    //block 3 top plane
    ($xAxis        $ymax_b2      $zAxis)//21
    ($xAxis        $ymax_b2      $zmid_b2)//22
    ($xmid_b2      $ymax_b2      $zmid_b2)//23
    ($xmid_b2      $ymax_b2      $zAxis)//24
    ($xAxis        $ymax_b2      $zmax_b2)//25
    ($xarc_b2      $ymax_b2      $zarc_b2)//26
    ($xmax_b2      $ymax_b2      $zAxis)//27
);

blocks
(
    hex (0 1 2 3 7 8 9 10) (5 5 50) simpleGrading (1 1 1)
    hex (2 5 6 3 9 12 13 10) (5 5 50) simpleGrading (1 1 1)
    hex (1 4 5 2 8 11 12 9) (5 5 50) simpleGrading (1 1 1)

    hex (7 8 9 10 14 15 16 17) (5 5 200) simpleGrading (1 1 8)
    hex (12 13 10 9 19 20 17 16) (5 5 200) simpleGrading (1 1 8)
    hex (8 11 12 9 15 18 19 16) (5 5 200) simpleGrading (1 1 8)

    hex (14 15 16 17 21 22 23 24) (5 5 50) simpleGrading (1 1 1)
    hex (19 20 17 16 26 27 24 23) (5 5 50) simpleGrading (1 1 1)
    hex (15 18 19 16 22 25 26 23) (5 5 50) simpleGrading (1 1 1)
);

edges
(
      arc  4 5   ($arc12x_b0 $ymin_b0 $arc12z_b0)//arc  1 2 (0.38268 0 0.923879)
      arc  11 12 ($arc12x_b0 $ymax_b0 $arc12z_b0)//arc  5 6 (0.38268 3 0.923879)
      arc  5 6   ($arc12z_b0 $ymin_b0 $arc12x_b0)//arc  2 3 (0.923879 0 0.38268)
      arc  12 13 ($arc12z_b0 $ymax_b0 $arc12x_b0)//arc  7 6 (0.923879 3 0.38268)
      arc  18 19 ($arc12x_b1 $ymax_b1 $arc12z_b1)
      arc  19 20 ($arc12z_b1 $ymax_b1 $arc12x_b1)
      arc  25 26 ($arc12x_b2 $ymax_b2 $arc12z_b2)
      arc  26 27 ($arc12z_b2 $ymax_b2 $arc12x_b2)
);

boundary
(
    inlet
    {
        type patch;
        faces
        (
            // (14 15 16 17) (15 18 19 16) (16 19 17 20)
            (21 22 23 24)(22 25 26 23)(23 26 27 24)
        );
    }
    // inlet_b0
    // {
    //     type    patch;
    //     (
    //         (7 8 9 10)
    //         (8 11 12 9)
    //         (9 12 13 10)
    //     )
    // }
    // outlet_b1
    // {
    //     type    patch;
    //     (
    //         (7 8 9 10)
    //         (8 11 12 9)
    //         (9 12 13 10)
    //     )
    // }

    outlet
    {
        type patch;
        faces
        (
            (0 3 2 1)(2 3 6 5)(1 2 5 4)
        );
    }
    lowerNozzleWall
    {
        type wall;
        faces
        (
            (5 6 13 12)(4 5 12 11)
        );
    }
    middleNozzleWall
    {
        type wall;
        faces
        (
            (12 13 20 19)(11 12 19 18)
        );
    }
    upperNozzleWall
    {
        type wall;
        faces
        (
            (19 20 27 26)(18 19 26 25)
        );
    }
    symmetryX
    {
        type symmetryPlane;
        faces
        (
            (7 10 3 0)(10 13 6 3)
            (14 17 10 7)(17 20 13 10)
            (21 24 17 14)(24 27 20 17)
        );
    }
    symmetryZ
    {
        type symmetryPlane;
        faces
        (
            (7 0 1 8)(8 1 4 11)
            (7 8 15 14)(8 11 18 15)
            (14 15 22 21)(15 18 25 22)
        );
    }
);

mergePatchPairs();

// ************************************************************************* //
