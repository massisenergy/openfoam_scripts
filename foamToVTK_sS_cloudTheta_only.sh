#! /bin/env bash
# author: massisenergy 
# short script to generate VTK with only shearStress and cloudTheta field 
# for space conservation

# copy 0/shearStress to all the directories listed using foamListTimes
foamListTimes | while read i ; do rsync -a 0/shearStress $i/ ; done;

# use `foamToVTK --help` for usage in details.
foamToVTK \
    -noPointValues \
    #-nearCellValue \
    -fields '(cloudTheta shearStress)';

find VTK/ -type d -empty -exec rm -rf '{}' \; #removes all empty directory

mv VTK VTK_sStresCTheta;
