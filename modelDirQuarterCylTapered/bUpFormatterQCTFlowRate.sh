#!/usr/bin/env bash
# ################################# Description ################################
# this awk script takes two `U,p` files needed for openFOAM, found in `0`
# directory of every project and formats a new U file, with input from
# a suitable `constant/polyMesh/boundary` file for assignement of
# patches (boundary conditions)
# NOTE: works with `blockMeshDict` blocks (25 100 1) simpleGrading (1 1 1)
# ##############################################################################

mv constant/polyMesh/boundary constant/polyMesh/boundary_backup;

# awk '$0!~"inGroups" {print;}
# ' constant/polyMesh/boundary_backup > constant/polyMesh/boundary_backup_1;
# awk '#NR<=19 {print;}
#     NR==21 && $0~"type" {gsub("patch;","patch;//inlet"); print;}
#     NR==27 && $0~"type" {gsub("patch;","patch;//outlet"); print;}#
#     # NR==51 && $0~"type" {gsub("wedge;","wedge;//wedgeBack"); print;}
#     # NR==58 && $0~"type" {gsub("wedge;","wedge;//wedgeFront"); print;}
#     $0~"type" && NR>27 {next;}
#     $0!~"inlet" || $0!~"outlet" || $0!~"nozzleWall" || $0!~"back" \
#     $0!~"front" || $0!~"symmetry" && NR>=21  {print;}
# ' constant/polyMesh/boundary_backup_1 | sed 's/wall;/wall;\/\/wall/g' | sed
#     's/symmetryPlane;/symmetryPlane;\/\/symmetryPlane/g' >
#     constant/polyMesh/boundary;

sed -e  '/inGroups/d' \
    -e '21s/patch;/patch;\/\/inlet/g' \
    -e '27s/patch;/patch;\/\/outlet/g' \
    -e 's/wall;/wall;\/\/wall/g' \
    -e 's/symmetryPlane;/symmetryPlane;\/\/symmetryPlane/g' \
    constant/polyMesh/boundary_backup > constant/polyMesh/boundary

###############################################################################

###############################################################################
# next two AWK scripts formats `U` & `p` respectively, using `boundary`.
# this is for fromatting the U file
awk 'BEGIN{
    print \
    "/*--------------------------------*- C++ -*----------------------------------*\\"\
    "\n  =========                 |"\
    "\n  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox"\
    "\n   \\    /   O peration     | Website:  https://openfoam.org"\
    "\n    \\  /    A nd           | Version:  9"\
    "\n     \\/     M anipulation  |"\
    "\n\\*---------------------------------------------------------------------------*/"\
    "\nFoamFile"\
    "\n{"\
    "\n    format      ascii;"\
    "\n    class       volVectorField;"\
    "\n    object      U;"\
    "\n}"\
    "\n// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //"\
    "\n"\
    "\ndimensions      [0 1 -1 0 0 0 0];"\
    "\n"\
    "\ninternalField   uniform (0 0 0);"\
    "\n"\
    "\nboundaryField"\
    "\n{"
}' > 0/U

awk 'BEGIN{FS=" "; flowRate = "1"}{
    if (NR < 19) {
        next;
    } else if ((NF == 2) && ($1 == "type") && ($2 ~ "inlet")) {
        print "\t\t"$1"\t\t\t""flowRateInletVelocity;"
        print "\t\t""volumetricFlowRate""\t\t\t""constant "flowRate";"
        print "\t\t""value""\t\t\t""uniform (0 0 0);"
    } else if ((NF == 2) && ($1 == "type") && ($2 ~ "outlet")) {
        print "\t\t"$1"\t\t\t""zeroGradient;"
    } else if ((NF == 2) && ($1 == "type") && ($2 ~ "wall;")) {
        print "\t\t"$1"\t\t\t""noSlip;"
    } else if ((NF == 2) && ($1 == "type") && ($2 ~ "symmetryPlane")) {
        gsub("empty;",";"); print;
    } else if ((NF == 2) && ($1 == "physicalType" || "nFaces" || "startFace")) {
        next;
    } else if ((NF == 3) && ($1 == "inGroups")) {
        next;
    } else if ((NF == 1) && ($0 == ")")) {
        print "}";
    } else {
        print;
    }
}' constant/polyMesh/boundary >> 0/U
################################################################################

################################################################################
#this is for fromatting the p file
awk 'BEGIN{
    print \
    "/*--------------------------------*- C++ -*----------------------------------*\\"\
    "\n  =========                 |"\
    "\n  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox"\
    "\n   \\    /   O peration     | Website:  https://openfoam.org"\
    "\n    \\  /    A nd           | Version:  9"\
    "\n     \\/     M anipulation  |"\
    "\n\\*---------------------------------------------------------------------------*/"\
    "\nFoamFile"\
    "\n{"\
    "\n    format      ascii;"\
    "\n    class       volScalarField;"\
    "\n    object      p;"\
    "\n}"\
    "\n// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //"\
    "\n"\
    "\ndimensions      [0 2 -2 0 0 0 0];"\
    "\n"\
    "\ninternalField   uniform 0;"\
    "\n"\
    "\nboundaryField"\
    "\n{"
}' > 0/p

awk 'BEGIN{FS=" "}{
    if (NR < 19) {
        next;
    } else if ((NF == 2) && ($1 == "type") && ($2 ~ "inlet")) {
        print "\t\t"$1"\t\t\t""zeroGradient;"

    } else if ((NF == 2) && ($1 == "type") && ($2 ~ "outlet")) {
        print "\t\t"$1"\t\t\t""fixedValue;"
        print "\t\t""value""\t\t\t""uniform 0;"
    } else if ((NF == 2) && ($1 == "type") && ($2 ~ "wall;")) {
        print "\t\t"$1"\t\t\t""zeroGradient;"
    } else if ((NF == 2) && ($1 == "type") && ($2 ~ "symmetryPlane")) {
        gsub("empty;","wedge;"); print;
    } else if ((NF == 2) && ($1 == "physicalType" || "nFaces" || "startFace")) {
        next;
    } else if ((NF == 3) && ($1 == "inGroups")) {
        next;
    } else if ((NF == 1) && ($0 == ")")) {
        print "}";
    } else {
        print;
    }
}' constant/polyMesh/boundary >> 0/p
