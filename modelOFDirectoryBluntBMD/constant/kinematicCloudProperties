/*--------------------------------*- C++ -*----------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     | Website:  https://openfoam.org
    \\  /    A nd           | Version:  8
     \\/     M anipulation  |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    location    "constant";
    object      kinematicCloudProperties;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

solution
{
    active          		true;
    calcFrequency               100;
    // maxTrackTime		100;//5.0;
    coupled         		false;
    transient       		yes;//off;
    cellValueSourceCorrection 	off;
    // maxCo           		   1;//0.3;

    sourceTerms
    {
        schemes
        {
        }
    }

    interpolationSchemes
    {
        rho             cell;
        // U               cellPoint;//leads to sticking error
        U               cellPointWallModified;//cellPoint;
        mu              cell;
    }

    integrationSchemes
    {
        U               Euler;
    }
}


constantProperties
{
    rho0            1000;//964;// density of particle, should be similar
    // to the density of the fluid for one way coupling.
    youngsModulus   1e8;//6e8;
    poissonsRatio   0.35;
}


subModels
{
    particleForces// see more in 2020Nourazi_particleTracer.pdf page12.
    {
        sphereDrag;
        gravity;//particleFoam needs constant/g file even if this is off.
    }

    injectionModels
    {//this part copied from cat ~/autoMount/Data_Documents/Dox_ownCloud/openFoamDataSR/E3DBP_Nozzle/CFD_E3DBP_PauloA_SM/PAA_Blunted_Nordson7018358_fR1.5microlitre/PAAB1_deltaX.02Y.025_autoAdjustedDeltaTnNIFOAM/PAAB1_deltaX.02Y.025_aADTnNIFOAM_steadyState/constant/kinematicCloudProperties
	injectionModel_patchInjection
	    {
            type                patchInjection;//banana;
            massTotal           250;//should be same number as for duration
            SOI                 0;// start of injection, ideally should start
            //when the fluid is steady to reduce computation, otherwise
            //starting at time 0 is not a problem.
            parcelBasisType     number;//fixed;//basis of particle per parcel calculation
            nParticle           1;// particles per parcel: used if parcelBasisTyps is fixed/number
            patchName           inlet;//injection patchname
            duration            250;// duration of injection
            parcelsPerSecond    2500;//1µL/sec fluid into quarterCyl, v_p=v_fluid*0.005238
            U0                  (0 0 0);  // initial velocity of parcels, for
            // one way coupling, whatever this value is, final particle velocity
            // (average) will be inherited from the fluid velocity when steady.
            flowRateProfile     uniform 1;//
	        sizeDistribution
            {
                type            fixedValue;
		        fixedValueDistribution
		        {
		            value	    1e-5;// diameter of all particles – 10µm
		        }
            }
    // oFT_simpleReactingParcelFoamVerticalChannel_model1
    //     {
    //         type            patchInjection;
    //         massFlowRate    0.8e-2;//0.8e-03;
    //         parcelBasisType mass;
    //         patchName       inletCentral;
    //         parcelsPerSecond 100;
    //         duration        1; // NOTE: set to 1 for steady state
    //         U0              (0 40 0);
    //         flowRateProfile constant 1;
	//         sizeDistribution
    //         {
    //             type            fixedValue;
	// 	        fixedValueDistribution
	// 	        {
	// 	            value	    1e-5;// diameter of all particles – 10µm
	// 	        }
    //         }
    //     }
	// injectionModel_flowRate
        // {
        //     type                patchFlowRateInjection; // injection model
        //     patchName           inlet;//injection patchname
        //     concentration       0.006;//Concentration profile of particle volume to carrier volume [-]
	//     parcelConcentration 1e13;//Parcels to introduce per unit volume flow rate m3 [n/m3]
	//     parcelBasisType     fixed;//basis of particle per parcel calculation
        //     // U0                  (0 0 0);  // initial velocity of parcels, for
        //     // one way coupling, whatever this value is, final particle velocity
        //     // (average) will be inherited from the fluid velocity when steady.
        //     nParticle           1; // particles per parcel, used if parcelBasisTyps is fixed
	//     sizeDistribution
        //     {
        //         type            fixedValue;
	// 	fixedValueDistribution
	// 	{
	// 	    value	1e-5;// diameter of all particles – 10µm
	// 	}
        //     }
	//     // flowRateProfile     constant 1;
        //     massTotal           0.1;//total mass to be injected (kg)
        //     SOI                 0;  //start of injection, ideally should start
        //     //when the fluid is steady to reduce computation, otherwise
        //     //starting at time 0 is not a problem.
        //     duration            1e9;  // duration of injection
        // }
	// injectionModel_Discharge
        // {
        //     type                manualInjection;//injection model
	//     parcelBasisType     fixed;         // basis of particle per parcel calculation
        //     U0                  (0 0 0);  // initial velocity of parcels, for
        //     // one way coupling, whatever this value is, final particle velocity
        //     // (average) will be inherited from the fluid velocity when steady.
        //     nParticle           1; // particles per parcel, used if parcelBasisTyps is fixed
        //     positionsFile       "kinematicCloudPositions";
	//     sizeDistribution
        //     {
        //         type            general;
        //         generalDistribution
        //         {
        //                distribution
	// 	       (
	// 		(4.0E-06	0.00000)
	// 		(8.0E-06	0.04817) 
	// 		(1.2E-05	0.02827)
	// 		(1.6E-05	0.16021)
	// 		(2.0E-05	0.45864)
	// 		(2.4E-05	0.22932)
	// 		(2.8E-05	0.05445)
	// 		(3.2E-05	0.01571)
	// 		(3.6E-05	0.00209)
	// 		(4.0E-05	0.00209)
	// 		(4.4E-05	0.00000)
	// 		(4.8E-05	0.00105)
	// 	       );
        //         }
        //     }
	//     // flowRateProfile     constant 1;
        //     massTotal           0;  // total mass to inject, not used in this settings
        //     SOI                 0;  // start of injection, ideally should start
        //     // when the fluid is steady to reduce computation, otherwise
        //     // starting at time 0 is not a problem.
        //     // duration            1e9;  // duration of injection
        // }
    }
}
    dispersionModel 		none;

    patchInteractionModel 	localInteraction;//none;
 
    localInteractionCoeffs
    {
        patches
        (
            //"(wall|baffle1|baffle2|baffle3|baffle4)"
            "(lowerNozzleWall|middleNozzleWall|upperNozzleWall)"
            {
                type            rebound;//banana
                e               1;  // normal restitution coefficient
                mu              0;  // tangential friction
            }
            //"(topInlet|outlet|bottomInlet)"
            "(inlet|outlet)"
            {
                type            escape;
            }
            "(symmetryX|symmetryZ)"//"(back|front)"
            {
                type            none;//escape;
            }
        );
    }
    

    surfaceFilmModel 		none;

    stochasticCollisionModel 	none;

    collisionModel 		none;//pairCollision;
    //this part is from $FOAM_TUTORIALS/lagrangian/particleFoam/hopper/hopperEmptying/constant/kinematicCloudProperties
    // pairCollisionCoeffs
    // {
    //     // Maximum possible particle diameter expected at any time
    //     maxInteractionDistance  0.006;

    //     writeReferredParticleCloud no;

    //     pairModel pairSpringSliderDashpot;

    //     pairSpringSliderDashpotCoeffs
    //     {
    //         useEquivalentSize   no;
    //         alpha               0.12;
    //         b                   1.5;
    //         mu                  0.52;
    //         cohesionEnergyDensity 0;
    //         collisionResolutionSteps 12;
    //     };

    //     wallModel    wallLocalSpringSliderDashpot;

    //     wallLocalSpringSliderDashpotCoeffs
    //     {
    //         useEquivalentSize no;
    //         collisionResolutionSteps 12;
    //         walls
    //         {
    //             youngsModulus   1e10;
    //             poissonsRatio   0.23;
    //             alpha           0.12;
    //             b               1.5;
    //             mu              0.43;
    //             cohesionEnergyDensity 0;
    //         }
    //         frontAndBack
    //         {
    //             youngsModulus   1e10;
    //             poissonsRatio   0.23;
    //             alpha           0.12;
    //             b               1.5;
    //             mu              0.1;
    //             cohesionEnergyDensity 0;
    //         }
    //     };
    // }
}


cloudFunctions
{
    // patchParticleHistogram1
    // {
    //     type                 patchParticleHistogram;
    //     patches
    //     (
    //         inlet
    //         upperNozzleWall
    //         middleNozzleWall
    //         lowerNozzleWall
    //         // humanBody
    //         outlet
    //     );
    //     nBins            9;
    //     min              5e-6;
    //     max              5e-5;
    //     maxStoredParcels 100000000;
    // }

    // patchPostProcessing1
    // {
    //     type            patchPostProcessing;
    //     maxStoredParcels 100;
    //     patches         ( outlet );
    // }

    // myParticleTracks
    // {
    //     type            particleTracks;
    //     trackInterval   5;
    //     maxSamples      1000000000;
    //     resetOnWrite    no;//yes;
    // }

    // myCloudFunction
    myVoidFraction
    // output is kinematicCloudTheta onto the Eulerian grid
    {
    	// type 		banana;
    	type 		voidFraction;
	    // banana		banana;
    }
}


// ************************************************************************* //
