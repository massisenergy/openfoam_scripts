/*--------------------------------*- C++ -*----------------------------------*\
=========                 |
\\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
\\    /   O peration     | Website:  https://openfoam.org
\\  /    A nd           | Version:  7
\\/     M anipulation  |
\*---------------------------------------------------------------------------*/
FoamFile
{
version     2.0;
format      ascii;
class       dictionary;
object      blockMeshDict;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

// Refernce:
convertToMeters 0.001;//`0.001` sets unit to millimeter.

// This part is for identification and modification of following parameters
// with external values, by programs such as `AWK`
Rb	2.135;
Rs	0.205;
Rm	1.725;
Lu	5;
Ll	0.76;
Lm	20; // only in 27G: NORDSONEFD-7018416 TIP 27GA RTT 008 CL.stl

// Minimum dimension: 0.05m (Rs, after doing `transformPoints`)
// For snappyHexMesh, blocks should ideally be cubes (X == Y == Z). So:
deltax_block0	     0.02;//0.005;
deltax_block1      $deltax_block0;
deltax_block2      $deltax_block0;
deltay_block0	     0.025;//0.01;
deltay_block1      $deltay_block0;
deltay_block2      $deltay_block1;

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

// coordinates
xAxis            0;
x_block0         $Rs;
ymin_block0      0;
ymax_block0      $Ll;

xmin_block1      $Rs;
xmax_block1      $Rm;
ymin_block1      $Ll;
ymax_block1      #calc "$Ll + $Lm";

xmin_block2      $Rm;
xmax_block2      $Rb;
ymin_block2      $ymax_block1;
ymax_block2      #calc "$Ll + $Lm + $Lu";

zmin                0;
zmax                0.1;

// Calculation of cell numbers
lx_block0           $Rs;
lx_block1           $Rm;
ly_block0           $Ll;
ly_block1           $Lu;
xcells_block0       #calc "round($lx_block0 / $deltax_block0 * 10)";
xcells_block1       #calc "round($lx_block1 / $deltax_block0 / 10 * 10)";
xcells_block2       $xcells_block1;
// ycells_block0       #calc "round(6.716/$deltay_block0)";
ycells_block0       #calc "round($Ll / $deltay_block0)";
ycells_block1       #calc "round($Lm / $deltay_block1 / 4 * 10)";
ycells_block2       #calc "round($Lu / $deltay_block2 / 10 * 10)";

// // Simplegrading expansion ratio
// expandRatio_yblock0     #calc "$Rm/$Rs";
// expandRatio_yblock1     #calc "$Rb/$Rm";

vertices
(//lowerNozzle
  ($xAxis      $ymin_block0    $zmin)//0
  ($xAxis      $ymin_block0    $zmax)//4
  ($x_block0   $ymin_block0    $zmax)//5
  ($x_block0   $ymin_block0    $zmin)//1
  ($xAxis      $ymax_block0    $zmin)//3
  ($xAxis      $ymax_block0    $zmax)//7
  ($x_block0   $ymax_block0    $zmax)//2
  ($x_block0   $ymax_block0    $zmin)//6
//middleNozzle
  ($xAxis         $ymin_block1    $zmin)//8
  ($xAxis         $ymin_block1    $zmax)//12
  ($xmin_block1   $ymin_block1    $zmax)//9 or
  ($xmin_block1   $ymin_block1    $zmin)//10 or
  ($xAxis         $ymax_block1    $zmin)//11
  ($xAxis         $ymax_block1    $zmax)//15
  ($xmax_block1   $ymax_block1    $zmax)//13
  ($xmax_block1   $ymax_block1    $zmin)//14
  //upperNozzle
  ($xAxis         $ymin_block2    $zmin)//8
  ($xAxis         $ymin_block2    $zmax)//12
  ($xmin_block2   $ymin_block2    $zmax)//9 or
  ($xmin_block2   $ymin_block2    $zmin)//10 or
  ($xAxis         $ymax_block2    $zmin)//11
  ($xAxis         $ymax_block2    $zmax)//15
  ($xmax_block2   $ymax_block2    $zmax)//13
  ($xmax_block2   $ymax_block2    $zmin)//14
);

blocks
(
hex (0 1 2 3 4 5 6 7)// (5 1 50) simpleGrading (1 1 1)
    (1 $xcells_block1 $ycells_block0)
    // simpleGrading (1 0.3 1) //original
    simpleGrading
    (
      1                  // x-direction expansion ratio
      (
        // (0.1 0.3 1)    // 20% y-dir, 30% cells, expansion = 1 4
        (0.6 0.45 1)    // 60% y-dir, 0.45 cells, expansion = 1
        (0.3 0.35 0.25) // 20% y-dir, 0.35 cells, expansion = 0.25 (1/4)
      )
      2                  // z(Y)-direction expansion ratio
    )
hex (8 9 10 11 12 13 14 15)// (5 1 50) simpleGrading (1 1 1)
    (1 $xcells_block1 $ycells_block1)
    // simpleGrading (1 0.3 5) //original
    // https://cfd.direct/openfoam/user-guide/v8-blockMesh#x26-1860005.3.1.4
    simpleGrading
    (
      1                  // x-direction expansion ratio
      (
          // (0.1 0.3 1)    // 20% y-dir, 30% cells, expansion = 1 4
          (0.6 0.45 1)    // 60% y-dir, 40% cells, expansion = 1
          (0.3 0.35 0.25) // 20% y-dir, 30% cells, expansion = 0.25 (1/4)
      )
      5                  // z(Y)-direction expansion ratio
    )
hex (16 17 18 19 20 21 22 23)// (5 50 1) simpleGrading (1 1 1)
    (1 $xcells_block2 $ycells_block2)
    // simpleGrading (1 0.3 1) //original
    simpleGrading
    (
      1
      ((0.6 0.45 1)  (0.3 0.35 0.25))
      1
    )
);

edges
(
);

boundary
(// first patches to be merged later in `mergePatchPairs`
  lowerNozzleEntry
  {
      type    patch;
      faces
      (          (4 5 6 7)
      );
  }
  middleNozzleExit
  {
      type    patch;
      faces
      (          (8 11 10 9)
      );
  }
  middleNozzleEntry
  {
      type    patch;
      faces
      (          (12 13 14 15)
      );
  }
  upperNozzleExit
  {
      type    patch;
      faces
      (        (16 19 18 17)
      );
  }

// patches to be kept for boundary condition & calculcations
inlet
{
    type patch;
    faces
    (        (20 21 22 23)
    );
}
outlet
{
    type patch;
    faces
    (        (0 3 2 1)
    );
}
lowerNozzleWall
{
    type wall;
    faces
    (        (3 7 6 2)
    );
}
middleNozzleWall
{
    type    wall;
    faces
    (        (11 15 14 10)
    );
}
upperNozzleWall
{
    type    wall;
    faces
    (        (19 23 22 18)
    );
}
axis
{
    type wall;
    faces
    (        (0 4 5 1)(8 12 13 9)(16 20 21 17)
    );
}
back
{
    type empty;
    faces
    (        (0 4 7 3)(8 12 15 11)(16 20 23 19)
    );
}
front
{
    type empty;
    faces
    (        (1 2 5 6)(9 13 14 10)(17 21 22 18)
    );
}
);

mergePatchPairs
(
      (upperNozzleExit      middleNozzleEntry)
      (middleNozzleExit     lowerNozzleEntry)
      // (upperNozzleLeft_interface   upperNozzleRight_interface)
);

// ************************************************************************* //
