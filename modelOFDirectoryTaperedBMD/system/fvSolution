/*--------------------------------*- C++ -*----------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     | Website:  https://openfoam.org
    \\  /    A nd           | Version:  8
     \\/     M anipulation  |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    location    "system";
    object      fvSolution;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

solvers
{
    p
    {
        solver                    GAMG;
        tolerance                 1e-06;
        relTol                    0.1;
        smoother                  GaussSeidel;
    }
/*
    U
    {
        type 			  coupled;
        solver 			  PBiCCCG;
        preconditioner 		  DILU;
        tolerance 		  (1e-8 1e-8 1e-8);
        relTol 			  (0.1 0.1 0.1);
    }
*/
    U
    {
        solver 			  smoothSolver;
        smoother 		  symGaussSeidel;
        tolerance 		  1e-05;
        relTol 			  0.1;
    }

    "(k|epsilon|omega|f|v2)"
    {
        solver                    smoothSolver;
        smoother                  symGaussSeidel;
        tolerance                 1e-05;
        relTol                    0.1;
    }
}

SIMPLE
{
    nNonOrthogonalCorrectors      0;
    consistent                    yes;
//For 2D cases, use two "U" residuals, e.g. "Ux and Uy", omitting the third.
//Play with p-residual, keeping the U-residuals constant, To achieve higher 
//accuracy of maximum values of pressure and sheer stress. 
    residualControl
    {
        p                         1e-6;//orignially 1e-2;
        Ux                        1e-8;//orignially 1e-3;
        Uy                        1e-8;//orignially 1e-3;
        // Uz                        1e-1;//orignially 1e-3;
        // U                         1e-5;//orignially 1e-3;
        "(k|epsilon|omega|f|v2)"  1e-3;//orignially 1e-3;
    }
}

relaxationFactors
// https://www.cfd-online.com/Forums/openfoam-solving/218363-what-exactly-relaxation-factor-does.html#post736869
// – 0: no under-relaxation (equivalent to not specified},
// - 0.2: very high stability but solution do not change much between
//subsequent iterations leading to large simulation time.
// – 1: matrix forced to become diagonally equal.
// - 0.95: Quick convergance but may lead to unstability (too much change
//betwteen subsequent iterations).
{
    equations
    {
        U                         0.95; // 0.9 is more stable but 0.95 more convergent
        ".*"                      0.95; // 0.9 is more stable but 0.95 more convergent
    }
}

// ************************************************************************* //
