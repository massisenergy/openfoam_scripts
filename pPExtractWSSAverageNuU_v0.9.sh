#!/usr/bin/env bash
################################### REFERENCE ################################
#https://unix.stackexchange.com/a/588325/359467
#https://stackoverflow.com/a/61869488/9592557
############################# SWITCH (User_input) ##############################
#input directory names that need to be processed
STUDYNAME=Alg340kPaBluntedRs
INPUTCSV=$STUDYNAME.csv
#ouput filenames
WSSMATCH=lowerNozzleWall;#name of the patch to calculate WSS
WSSFILE="./postProcessing/WSS/0/wallShearStress.dat";
AVGNUUFILE=\
"./postProcessing/patchAverage\(name=outlet,U,nu\)/0/surfaceFieldValue.dat";
FLOWRATEFILE=\
"./postProcessing/flowRatePatch\(name=outlet\)/0/surfaceFieldValue.dat";
FLUXMATCH=NONE;
PPWSS=pPWSS_$STUDYNAME;
PPFLUXOUTLET=pPfluxOutlet_$STUDYNAME;
PPAVGNUU=pPAvgNuU_$STUDYNAME;

# cd $STUDYNAME || exit;
########################### SWITCH (User_input) END ############################

# extract the maximum values of wallShearStress at the latest time
awk -F, 'NR >= 2 && NR <= 2506 {print $0}' $INPUTCSV |#choose rows
while IFS="," read -r DIRECTORY ; do
    cd "$DIRECTORY" && echo $PWD;

    awk -v DIRECTORY=$DIRECTORY -v WSSMATCH="$WSSMATCH" '
        NR==FNR && $0 ~ WSSMATCH {LastLine=FNR; next;}
        FNR==LastLine {gsub(/\(/,"",$0); gsub(/(\/.+)/,"",DIRECTORY);
        magWSS=(($6**2+$7**2)**0.5);
        print DIRECTORY"\t"$1"\t"$2"\t"magWSS}' $WSSFILE $WSSFILE \
    >> ../$PPWSS.csv;

    awk -v DIRECTORY=$DIRECTORY 'NR==FNR{LastLine=FNR; next;}
        FNR==LastLine {gsub(/\(/,"",$0); gsub(/(\/.+)/,"",DIRECTORY);
        flowRateOut=($2*72*1e9);
        print DIRECTORY"\t\t"$1"\t"$2"\t"flowRateOut}' $FLOWRATEFILE \
        $FLOWRATEFILE \
    >> ../$PPFLUXOUTLET.csv;

    awk -v DIRECTORY=$DIRECTORY 'NR==FNR{LastLine=FNR; next;}
        FNR==LastLine {gsub(/\(|\)/,"",$0); gsub(/(\/.+)/,"",DIRECTORY);
        magU=(($2**2+$3**2)**0.5);
        print DIRECTORY"\t\t"$1"\t"$2"\t"$3"\t"$5"\t"magU}' $AVGNUUFILE \
        $AVGNUUFILE \
    >> ../$PPAVGNUU.csv;
    cd ..;
done;

######################### Adding the header manually ##########################
printf \
"%s\n" $'0a\nMaterial_DP\tRs\tsimulationTime\tWSS_X\tWSS_Y\tmagnitudeWSS(Pa)' \
. x | ex -s $PPWSS.csv;
printf "%s\n" 0a \
$'Material_DP\tRs\tsimulationTime\tflux(ms-1)\tflowRate(µLs-1)' \
. x | ex -s $PPFLUXOUTLET.csv;
printf "%s\n" 0a \
$'Material_DP\tRs\tsimulationTime\tavgUx(ms-1)\tavgUy(ms-1)\tavgNuOutlet(m2s-1)\tmagAvgUOutlet(ms-1)' \
. x | ex -s $PPAVGNUU.csv;

###############################################################################
