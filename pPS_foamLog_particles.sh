#!/bin/bash
# extract the maximum values of f at the latest time postProcessing

################################# User_input ###################################
#input directory names that need to be processed
folder=PF127*/;#example: <PF127*/>

################################################################################

while IFS= read -e "Enter foamLog.* filename"  files; do 
    foamLogFile='foamLog.22GTQC_nCPFoam_massTotal1.06194888E-07_parcelsPerSecond2500_duration160';
    grep -e "^Time = " $foamLogFile | awk -F= '{print $NF}' | sed '1i \ Time' > Time;
    grep -e "Parcel fate (number, mass)      : patch (inlet|outlet)" $foamLogFile -A1 | awk -F= '/escape/{print $NF}' | awk -F, '{print $2}' | sed '1i \ Mass_escapeInletOutlet' > Mass_escapeInletOutlet;
    grep -e "Parcel fate (number, mass)      : patch (inlet|outlet)" $foamLogFile -A1 | awk -F= '/escape/{print $NF}' | awk -F, '{print $1}' | sed '1i \ N_escapeInletOutlet' > N_escapeInletOutlet;
    grep -e "number of parcels added" $foamLogFile | awk -F= '{print $2}' | sed '1i \ N_parcelAdded' > N_parcelAdded;
    grep -e "mass introduced" $foamLogFile | awk -F= '{print $2}' | sed '1i \ massIntroduced' > massIntroduced;
    grep -e "Current number of parcels" $foamLogFile | awk -F= '{print $2}' | sed '1i \ N_parcelCurrent' > N_parcelCurrent;
    grep -e "Current mass in system" $foamLogFile | awk -F= '{print $2}' | sed '1i \ massCurrent' > massCurrent;


    paste Time N_escapeInletOutlet | column -s ',' -t > 1.csv;
    paste 1.csv Mass_escapeInletOutlet | column -s $',' -t > 2.csv;
    paste 2.csv N_parcelAdded | column -s $',' -t > 3.csv;
    paste 3.csv massIntroduced | column -s $',' -t > 4.csv;
    paste 4.csv N_parcelCurrent | column -s $',' -t > 5.csv;
    paste 4.csv massCurrent | column -s $',' -t > 5.csv;

done;
